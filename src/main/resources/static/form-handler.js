const formHandler = {
    mainContent: document.getElementById('main-content'),

    handle: function () {
        document.getElementById('new-teacher').addEventListener('click', this.createNewTeacherForm.bind(this));
        document.getElementById('new-student').addEventListener('click', this.createNewStudentForm.bind(this));
        document.getElementById('new-discipline').addEventListener('click', this.createNewDisciplineForm.bind(this));
        document.getElementById('enroll-student').addEventListener('click', this.createEnrollStudentForm.bind(this));
        document.getElementById('disenroll-student').addEventListener('click', this.createDisenrollStudentForm.bind(this))
    },

    createNewTeacherForm: function () {
        this.clearMainContent();
        this.buildFormSkeleton();

        document.getElementsByClassName('form-heading')[0].innerText = 'Register Teacher';

        this.buildInputField();
        this.buildDropDownField('teacher-title-btn', 'teacher-title-content');

        document.getElementById('teacher-title-btn').innerText = 'Select Title';

        this.buildDropdownFieldContent('teacher-title-btn', 'teacher-title-content',
            ['Assistant professor', 'Chief assistant professor', 'Docent professor', 'Professor'])

        this.buildSubmitButton('http://localhost:8080/api/teachers', 'post', {
            name: document.getElementById('form-input'),
            title: document.getElementById('teacher-title-btn')
        }, this.isNewTeacherDataInvalid);
    },

    isNewTeacherDataInvalid: function (parsedBody) {
        if (parsedBody.title === 'ASSISTANT_PROFESSOR') return false;
        if (parsedBody.title === 'CHIEF_ASSISTANT_PROFESSOR') return false;
        if (parsedBody.title === 'DOCENT_PROFESSOR') return false;
        if (parsedBody.title === 'PROFESSOR') return false;
        return 'Invalid teacher title';
    },

    createNewStudentForm: function () {
        this.clearMainContent();
        this.buildFormSkeleton();

        document.getElementsByClassName('form-heading')[0].innerText = 'Register Student';

        this.buildInputField();
        this.buildDropDownField('student-course-btn', 'student-course-content');

        document.getElementById('student-course-btn').innerText = 'Select Course';

        this.buildDropdownFieldContent('student-course-btn', 'student-course-content',
            [1, 2, 3, 4])

        this.buildSubmitButton('http://localhost:8080/api/students', 'post', {
            name: document.getElementById('form-input'),
            course: document.getElementById('student-course-btn')
        }, this.isNewStudentDataInvalid);
    },

    isNewStudentDataInvalid: function (parsedBody) {
        if (parsedBody.course == 1) return false;
        if (parsedBody.course == 2) return false;
        if (parsedBody.course == 3) return false;
        if (parsedBody.course == 4) return false;
        return 'Invalid student course';
    },

    createNewDisciplineForm: async function () {
        this.clearMainContent();
        this.buildFormSkeleton();

        document.getElementsByClassName('form-heading')[0].innerText = 'Register Discipline';

        this.buildInputField();

        const teachers = await requestHandler.request('http://localhost:8080/api/teachers', 'get');

        this.buildDropDownField('discipline-teacher-btn', 'discipline-teacher-content');
        document.getElementById('discipline-teacher-btn').innerText = 'Select Teacher';
        this.buildDropdownFieldContent('discipline-teacher-btn', 'discipline-teacher-content', teachers)

        this.buildDropDownField('discipline-credit-btn', 'discipline-credit-content');
        document.getElementById('discipline-credit-btn').innerText = 'Select Credit';
        this.buildDropdownFieldContent('discipline-credit-btn', 'discipline-credit-content',
            [1, 2, 3, 4, 5])

        this.buildSubmitButton('http://localhost:8080/api/disciplines', 'post', {
            name: document.getElementById('form-input'),
            teacherId: document.getElementById('discipline-teacher-btn'),
            credit: document.getElementById('discipline-credit-btn')
        }, this.isNewDisciplineDataInvalid.bind(this));
    },

    isNewDisciplineDataInvalid: async function (parsedBody) {
        const teachers = await requestHandler.request('http://localhost:8080/api/teachers', 'get');
        const teacher = this.getBy(parsedBody.teacherId, 'id', teachers);
        if (!teacher) return 'Teacher does not exist';

        const disciplines = await requestHandler.request('http://localhost:8080/api/disciplines', 'get');

        for (const d of disciplines)
            if (d.name === parsedBody.name)
                return 'Discipline with name ' + parsedBody.name + ' already exists';


        if (parsedBody.credit == 1) return false;
        if (parsedBody.credit == 2) return false;
        if (parsedBody.credit == 3) return false;
        if (parsedBody.credit == 4) return false;
        if (parsedBody.credit == 5) return false;

        return 'Invalid discipline credit';
    },

    getBy: function (id, propertyIdName, resources) {
        for (let resource of resources)
            if (id === resource[propertyIdName])
                return resource;

        return false;
    },

    createEnrollStudentForm: async function () {
        this.clearMainContent();
        this.buildFormSkeleton();

        document.getElementsByClassName('form-heading')[0].innerText = 'Register Student in a Discipline';

        const students = await requestHandler.request('http://localhost:8080/api/students/courses', 'get');

        this.buildDropDownField('enroll-student-btn', 'enroll-student-content');
        document.getElementById('enroll-student-btn').innerText = 'Select Student';
        this.buildDropdownFieldContent('enroll-student-btn', 'enroll-student-content', students);

        const disciplines = await requestHandler.request('http://localhost:8080/api/disciplines', 'get');

        this.buildDropDownField('enroll-discipline-btn', 'enroll-discipline-content');
        document.getElementById('enroll-discipline-btn').innerText = 'Select Discipline';
        this.buildDropdownFieldContent('enroll-discipline-btn', 'enroll-discipline-content', disciplines)

        this.buildSubmitButton('http://localhost:8080/api/students-disciplines', 'post', {
            studentId: document.getElementById('enroll-student-btn'),
            disciplineId: document.getElementById('enroll-discipline-btn')
        }, this.isNewEnrollmentInvalid.bind(this));
    },

    isNewEnrollmentInvalid: async function (parsedBody) {
        const students = await requestHandler.request('http://localhost:8080/api/students/disciplines', 'get');
        const student = this.getBy(parsedBody.studentId, 'id', students);
        if (!student) return 'Student does not exist';

        const disciplines = await requestHandler.request('http://localhost:8080/api/disciplines', 'get');
        const discipline = await this.getBy(parsedBody.disciplineId, 'id', disciplines);
        if (!discipline) return 'Discipline does not exist';

        const studentDisciplines = student.disciplines;

        for (const d of studentDisciplines)
            if (d.id === parsedBody.disciplineId)
                return 'Student is already enrolled';

        return false;
    },

    createDisenrollStudentForm: async function () {
        this.clearMainContent();
        this.buildFormSkeleton();

        document.getElementsByClassName('form-heading')[0].innerText = 'Unregister Student from a Discipline';

        const students = await requestHandler.request('http://localhost:8080/api/students/courses', 'get');

        this.buildDropDownField('disenroll-student-btn', 'disenroll-student-content');
        document.getElementById('disenroll-student-btn').innerText = 'Select Student';
        this.buildDropdownFieldContent('disenroll-student-btn', 'disenroll-student-content', students);

        const disciplines = await requestHandler.request('http://localhost:8080/api/disciplines', 'get');

        this.buildDropDownField('disenroll-discipline-btn', 'disenroll-discipline-content');
        document.getElementById('disenroll-discipline-btn').innerText = 'Select Discipline';
        this.buildDropdownFieldContent('disenroll-discipline-btn', 'disenroll-discipline-content', disciplines);

        this.buildSubmitButton('http://localhost:8080/api/students-disciplines', 'delete', {
            studentId: document.getElementById('disenroll-student-btn'),
            disciplineId: document.getElementById('disenroll-discipline-btn')
        }, this.isNewDisEnrollmentInvalid.bind(this));
    },

    isNewDisEnrollmentInvalid: async function (parsedBody) {
        const students = await requestHandler.request('http://localhost:8080/api/students/disciplines', 'get');
        const student = this.getBy(parsedBody.studentId, 'id', students);
        if (!student) return 'Student does not exist';

        const disciplines = await requestHandler.request('http://localhost:8080/api/disciplines', 'get');
        const discipline = await this.getBy(parsedBody.disciplineId, 'id', disciplines);
        if (!discipline) return 'Discipline does not exist';

        const studentDisciplines = student.disciplines;

        for (const d of studentDisciplines)
            if (d.id === parsedBody.disciplineId)
                return false;

        return 'Student is not enrolled';
    },

    clearMainContent: function () {
        while (this.mainContent.firstChild)
            this.mainContent.removeChild(this.mainContent.lastChild);
    },

    buildFormSkeleton: function () {
        const formWrapper = document.createElement('div');
        formWrapper.className = 'form-wrapper';

        const form = document.createElement('form');
        form.className = 'form';

        const formHeading = document.createElement('h1');
        formHeading.className = 'form-heading';

        this.mainContent.appendChild(formWrapper);
        formWrapper.appendChild(form);
        form.appendChild(formHeading)
    },

    buildInputField: function () {
        const inputField = document.createElement('input');
        inputField.type = 'text';
        inputField.autocomplete = 'off';
        inputField.className = 'input';
        inputField.placeholder = 'Add Name';
        inputField.id = 'form-input';
        inputField.style.color = 'rgba(0, 0, 0, 0.7)';

        inputField.addEventListener('focus', function () {
            inputField.placeholder = '';
        })

        document.getElementsByClassName('form')[0].appendChild(inputField);
    },

    buildDropDownField(dropDownButtonId, dropDownContentId) {
        const dropDownWrapper = document.createElement('div');
        dropDownWrapper.className = 'dropdown';

        const dropDownButton = document.createElement('div');
        dropDownButton.className = 'dropdown-button';
        dropDownButton.id = dropDownButtonId;
        dropDownButton.style.paddingTop = '20px';
        dropDownButton.style.color = 'rgba(0, 0, 0, 0.4)';
        dropDownButton.style.margin = '20px 0';

        dropDownButton.addEventListener('mouseover', function () {
            dropDownButton.style.backgroundColor = '#f1f1f1';
        })

        dropDownButton.addEventListener('mouseleave', function () {
            dropDownButton.style.backgroundColor = '#f9f9f9';
        })

        dropDownButton.addEventListener('click', () => {
            this.changeElementDisplay(dropDownContentId);
        })

        const dropDownContent = document.createElement('div');
        dropDownContent.className = 'nav-content';
        dropDownContent.id = dropDownContentId;
        dropDownContent.style.width = '420px';
        dropDownContent.style.maxHeight = '250px';
        dropDownContent.style.overflow = 'auto';

        dropDownWrapper.appendChild(dropDownButton);
        dropDownWrapper.appendChild(dropDownContent);

        document.getElementsByClassName('form')[0].appendChild(dropDownWrapper);
    },

    buildDropdownFieldContent: function (dropdownButtonId, dropdownContentId, contentValues) {
        for (let i = 0; i < contentValues.length; i++) {
            const content = document.createElement('a');
            content.innerText = contentValues[i].name ? 'ID:' + contentValues[i].id + '  ' + contentValues[i].name : contentValues[i];

            document.getElementById(dropdownContentId).appendChild(content);

            content.addEventListener('click', () => {
                const dropdownButton = document.getElementById(dropdownButtonId);

                if (contentValues[i].name) dropdownButton.customId = contentValues[i].id;

                dropdownButton.innerText = content.innerText;

                this.changeElementDisplay(dropdownContentId);
            })
        }
    },

    changeElementDisplay: function (id) {
        const element = document.getElementById(id);

        if (element.style.display === 'block') element.style.display = 'none';
        else element.style.display = 'block';
    },

    appendSuccessMessage: function () {
        this.clearMainContent();

        const msg = document.createElement('h1');
        msg.className = 'form-heading';
        msg.innerText = 'Record Successful!'

        this.mainContent.appendChild(msg);
    },

    buildSubmitButton: function (url, method, body, isInvalid) {
        const submitButton = document.createElement('span');
        submitButton.className = 'input';
        submitButton.id = 'form-submit-button';
        submitButton.innerText = 'SUBMIT';
        submitButton.style.color = 'rgba(0, 0, 0, 0.4)';
        submitButton.style.display = 'flex';
        submitButton.style.justifyContent = 'center';
        submitButton.style.alignItems = 'center';

        submitButton.addEventListener('mouseover', function () {
            submitButton.style.backgroundColor = 'rgba(127, 255, 212, 1)';
        })

        submitButton.addEventListener('mouseleave', function () {
            submitButton.style.backgroundColor = 'rgba(127, 255, 212, 0.7)';
        })

        submitButton.addEventListener('click', async () => {
            const parsedBody = this.parse(body);

            if (!this.areFieldsFilled(parsedBody)) {
                this.buildErrorMessage('All fields must be filled');
                return;
            }

            let errorMessage = await isInvalid(parsedBody);

            if (errorMessage) {
                this.buildErrorMessage(errorMessage);
                return;
            }

            requestHandler.request(url, method, parsedBody)
                .then(this.appendSuccessMessage.bind(this))
                .catch((message) => {
                    message = message.startsWith('JSON parse error') ? 'All fields must be filled' : message;
                    this.buildErrorMessage(message);
                });
        })

        document.getElementsByClassName('form')[0].appendChild(submitButton);
    },

    areFieldsFilled: function (parsedBody) {
        for (let value of Object.values(parsedBody))
            if (value === '' || value.toString().startsWith('Select'))
                return false;

        return true;
    },

    buildErrorMessage: function (message) {
        if (document.getElementById('error-message')) {
            document.getElementById('error-message').innerText = message;
            return;
        }

        const errorMessage = document.createElement('p');
        errorMessage.id = 'error-message';
        errorMessage.style.color = 'rgba(255,0,0,0.6)';
        errorMessage.style.fontSize = '20px';
        errorMessage.innerText = message;

        document.getElementsByClassName('form')[0].appendChild(errorMessage);
    },

    parse: function (body) {
        const parsedBody = {};

        for (let [key, value] of Object.entries(body))
            if (key === 'name')
                parsedBody[key] = value.value;
            else if (key === 'title')
                parsedBody[key] = value.innerText.toUpperCase().split(' ').join('_');
            else
                parsedBody[key] = value.customId ? value.customId : value.innerText;

        return parsedBody;
    }
}
const tableHandler = {
    mainContent: document.getElementById('main-content'),

    handle: function () {
        document.getElementById('teachers-and-students').addEventListener('click', this.createTeachersAndStudentsTable.bind(this));
        document.getElementById('teachers').addEventListener('click', this.createTeachersTable.bind(this));
        document.getElementById('students-disciplines').addEventListener('click', this.createStudentsDisciplinesTable.bind(this));
        document.getElementById('students-credits').addEventListener('click', this.createStudentsCreditsTable.bind(this));
        document.getElementById('top-teachers-and-disciplines').addEventListener('click', this.createTopTeachersAndDisciplinesTable.bind(this));
        document.getElementById('disciplines-having-one-or-more-students').addEventListener('click', this.createDisciplinesHavingMoreThanOneStudentTable.bind(this));
    },

    createTeachersAndStudentsTable: async function () {
        this.clearMainContent();

        const teachers = await requestHandler.request('http://localhost:8080/api/teachers/disciplines-count', 'get');

        this.buildTableSkeleton('TEACHERS AND NUMBER OF DISCIPLINES TAUGHT', ['ID', 'NAME', 'NUMBER OF DISCIPLINES TAUGHT'], 'teachers-table');

        for (let teacher of teachers)
            this.buildNextRow([teacher.id, teacher.name, teacher.disciplinesCount], 'teachers-table');

        const students = await requestHandler.request('http://localhost:8080/api/students/courses', 'get');

        this.buildTableSkeleton('STUDENTS AND THEIR COURSE', ['ID', 'NAME', 'COURSE'], 'students-table');

        for (let student of students)
            this.buildNextRow([student.id, student.name, student.course], 'students-table');
    },

    createTeachersTable: async function () {
        this.clearMainContent();

        const teachers = await requestHandler.request('http://localhost:8080/api/teachers/disciplines/students-count', 'get');

        this.buildTableSkeleton('TEACHERS AND NAMES OF DISCIPLINES TAUGHT', ['ID', 'NAME', 'NAMES OF DISCIPLINES TAUGHT'], 'teachers-table');

        for (let teacher of teachers)
            this.buildNextRow([teacher.id, teacher.name, this.extractDisciplinesNames(teacher, true)], 'teachers-table');
    },

    createStudentsDisciplinesTable: async function () {
        this.clearMainContent();

        const students = await requestHandler.request('http://localhost:8080/api/students/disciplines', 'get');

        this.buildTableSkeleton('STUDENTS AND THEIR DISCIPLINES', ['ID', 'NAME', 'DISCIPLINES'], 'students-table');

        for (let student of students)
            this.buildNextRow([student.id, student.name, this.extractDisciplinesNames(student)], 'students-table');
    },

    createStudentsCreditsTable: async function () {
        this.clearMainContent();

        const students = await requestHandler.request('http://localhost:8080/api/students/credits', 'get');

        this.buildTableSkeleton('STUDENTS AND THEIR CREDITS', ['ID', 'NAME', 'CREDITS'], 'students-table');

        for (let student of students)
            this.buildNextRow([student.id, student.name, student.creditsCount], 'students-table');
    },

    createTopTeachersAndDisciplinesTable: async function () {
        this.clearMainContent();

        let teachers = await requestHandler.request('http://localhost:8080/api/teachers/top-three', 'get');

        this.buildTableSkeleton('TOP THREE TEACHERS HAVING MOST STUDENTS', ['ID', 'NAME', 'NUMBER OF STUDENTS'], 'top-three-teachers-table');

        for (let teacher of teachers)
            this.buildNextRow([teacher.id, teacher.name, teacher.studentsCount], 'top-three-teachers-table');

        let disciplines = await requestHandler.request('http://localhost:8080/api/disciplines/top-three', 'get');

        this.buildTableSkeleton('TOP THREE DISCIPLINES HAVING MOST STUDENTS', ['ID', 'NAME', 'NUMBER OF STUDENTS'], 'top-three-disciplines-table');

        for (let discipline of disciplines)
            this.buildNextRow([discipline.id, discipline.name, discipline.studentsCount], 'top-three-disciplines-table');
    },

    createDisciplinesHavingMoreThanOneStudentTable: async function() {
        this.clearMainContent();

        let disciplines = await requestHandler.request('http://localhost:8080/api/disciplines/having-one-or-more-students', 'get');

        this.buildTableSkeleton('DISCIPLINES HAVING MORE THAN ONE STUDENT', ['ID', 'NAME', 'NUMBER OF STUDENTS'], 'disciplines-having-one-or-more-students-table');

        for (let discipline of disciplines)
            this.buildNextRow([discipline.id, discipline.name, discipline.studentsCount], 'disciplines-having-one-or-more-students-table');
    },

    extractDisciplinesNames: function (facultyMember, studentsCount) {
        let disciplinesNames = [];

        for (let discipline of facultyMember.disciplines)
            if (studentsCount)
                disciplinesNames.push(discipline.name + ' (' + discipline.studentsCount + ' students)');
            else
                disciplinesNames.push(discipline.name);

        return disciplinesNames.join(', ');
    },

    buildTableSkeleton: function (titleValue, headings, id) {
        const tableWrapper = document.createElement('div');
        tableWrapper.className = 'table-wrapper';

        const table = document.createElement('table');
        table.className = 'table';
        table.id = id;

        const title = document.createElement('h1');
        title.className = 'table-heading';
        title.innerText = titleValue;
        this.mainContent.appendChild(title);

        const headingRow = document.createElement('tr');
        this.appendRowValues(headingRow, headings, 'th');

        table.appendChild(headingRow);
        tableWrapper.appendChild(table);
        this.mainContent.appendChild(tableWrapper);
    },

    buildNextRow: function (values, tableId) {
        const row = document.createElement('tr');
        this.appendRowValues(row, values, 'td');

        document.getElementById(tableId).appendChild(row);
    },

    appendRowValues(row, values, type) {
        for (let i = 0; i < values.length; i++) {
            const element = document.createElement(type);
            element.innerText = values[i];
            row.appendChild(element);
        }
    },

    clearMainContent: function () {
        while (this.mainContent.firstChild)
            this.mainContent.removeChild(this.mainContent.lastChild);
    }
}
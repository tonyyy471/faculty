const requestHandler = {
    request: function (url, method, body) {
        return new Promise((resolve, reject) => {
            this.buildRequest(url, method, body, resolve, reject);
        })
    },

    buildRequest: function (url, method, body, resolve, reject) {
        const request = new XMLHttpRequest();

        request.open(method, url);
        request.onload = function () {
            if (this.status >= 200 && this.status < 300)
                resolve(method === 'get' ? JSON.parse(this.responseText) : this.responseText);
             else
                reject(JSON.parse(this.responseText).message);
        };
        request.onerror = function () {
            reject(JSON.parse(this.responseText).message);
        };

        if (body) {
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(JSON.stringify(body));
        } else
            request.send();
    }
}
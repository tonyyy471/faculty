package com.example.university.services;

import com.example.university.models.Student;
import com.example.university.models.StudentDiscipline;
import com.example.university.models.Teacher;
import com.example.university.models.dtos.FacultyEntityDTO;

import java.sql.SQLException;
import java.util.List;

public interface FacultyMemberService {
    void register(Teacher teacher) throws SQLException;

    void register(Student student) throws SQLException;

    void enroll(StudentDiscipline studentDiscipline) throws SQLException;

    void disenroll(StudentDiscipline studentDiscipline) throws SQLException;

    List<FacultyEntityDTO> getTeachersAndTheirDisciplinesCount() throws SQLException;

    List<FacultyEntityDTO> getStudentsAndTheirCourses() throws SQLException;

    List<FacultyEntityDTO> getStudentsAndTheirDisciplines() throws SQLException;

    List<FacultyEntityDTO> getStudentsAndTheirCredits() throws SQLException;

    List<FacultyEntityDTO> getTeachersTheirDisciplinesAndTheirStudentsCount() throws SQLException;

    List<FacultyEntityDTO> getTopThreeTeachers() throws SQLException;

    List<FacultyEntityDTO> getTeachers()throws SQLException;
}

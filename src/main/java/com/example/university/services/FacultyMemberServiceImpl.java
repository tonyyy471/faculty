package com.example.university.services;

import com.example.university.models.*;
import com.example.university.models.dtos.FacultyEntityDTO;
import com.example.university.repositiories.DisciplineRepository;
import com.example.university.repositiories.FacultyMemberRepository;
import com.example.university.services.utils.Verifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

@Service
public class FacultyMemberServiceImpl implements FacultyMemberService {
    private final FacultyMemberRepository facultyMemberRepository;
    private final DisciplineRepository disciplineRepository;

    @Autowired
    public FacultyMemberServiceImpl(FacultyMemberRepository facultyMemberRepository, DisciplineRepository disciplineRepository) {
        this.facultyMemberRepository = facultyMemberRepository;
        this.disciplineRepository = disciplineRepository;
    }

    @Override
    public void register(Teacher teacher) throws SQLException {
        String name = teacher.getName();
        Verifier.verify().isNotNull(name, "Teacher name is not provided");
        Verifier.verify().isNotEmptyOrBlank(name, "Teacher name cannot be empty or blank");

        Title title = teacher.getTitle();
        Verifier.verify().isNotNull(String.valueOf(title), "Teacher title is not provided");
        Verifier.verify().isNotEmptyOrBlank(String.valueOf(title), "Teacher title cannot be empty or blank");

        facultyMemberRepository.save(teacher);
    }

    @Override
    public void register(Student student) throws SQLException {
        String name = student.getName();
        Verifier.verify().isNotNull(name, "Student name is not provided");
        Verifier.verify().isNotEmptyOrBlank(name, "Student name cannot be empty or blank");

        Integer course = student.getCourse();
        Verifier.verify().isNotNull(course, "Student course is not provided");
        Verifier.verify().isInRange(course, 1, 4, "Student course cannot be less than %d and more than %d");

        facultyMemberRepository.save(student);
    }

    @Override
    public void enroll(StudentDiscipline studentDiscipline) throws SQLException {
        verify(studentDiscipline, true);

        facultyMemberRepository.save(studentDiscipline);
    }

    @Override
    public void disenroll(StudentDiscipline studentDiscipline) throws SQLException {
        verify(studentDiscipline, false);

        facultyMemberRepository.delete(studentDiscipline);
    }

    @Override
    public List<FacultyEntityDTO> getTeachersAndTheirDisciplinesCount() throws SQLException {
        List<FacultyEntityDTO> teachers = facultyMemberRepository.getTeachersAndTheirDisciplinesCount();
        return teachers;
    }

    @Override
    public List<FacultyEntityDTO> getStudentsAndTheirCourses() throws SQLException {
        List<FacultyEntityDTO> students = facultyMemberRepository.getStudentsAndTheirCourses();
        return students;
    }

    @Override
    public List<FacultyEntityDTO> getStudentsAndTheirDisciplines() throws SQLException{
        List<FacultyEntityDTO> students = facultyMemberRepository.getStudentsAndTheirDisciplines();
        return students;
    }

    @Override
    public List<FacultyEntityDTO> getStudentsAndTheirCredits() throws SQLException {
        List<FacultyEntityDTO> students = facultyMemberRepository.getStudentsAndTheirCredits();
        return students;
    }

    @Override
    public List<FacultyEntityDTO> getTeachersTheirDisciplinesAndTheirStudentsCount() throws SQLException {
        List<FacultyEntityDTO> teachers = facultyMemberRepository.getTeachersTheirDisciplinesAndTheirStudentsCount();
        return teachers;
    }

    @Override
    public List<FacultyEntityDTO> getTopThreeTeachers() throws SQLException {
        List<FacultyEntityDTO> teachers = facultyMemberRepository.getTopThreeTeachers();
        return teachers;
    }

    @Override
    public List<FacultyEntityDTO> getTeachers() throws SQLException {
        List<FacultyEntityDTO> teachers = facultyMemberRepository.getTeachers();
        return teachers;
    }

    private void verify(StudentDiscipline studentDiscipline, boolean forEnroll) throws SQLException {
        Integer studentId = studentDiscipline.getStudentId();
        Verifier.verify().isNotNull(studentId, "Student id is not provided");

        Integer disciplineId = studentDiscipline.getDisciplineId();
        Verifier.verify().isNotNull(disciplineId, "Discipline id is not provided");

        Student student = facultyMemberRepository.getStudentBy(studentId);
        Verifier.verify().isNotNull(student, "Student does not exist");

        Discipline discipline = disciplineRepository.getDisciplineBy(disciplineId);
        Verifier.verify().isNotNull(discipline, "Discipline does not exist");

        StudentDiscipline existingStudentDiscipline =
                facultyMemberRepository.getStudentDisciplineBy(studentDiscipline.getStudentId(), studentDiscipline.getDisciplineId());
        if (forEnroll)
            Verifier.verify().isNull(existingStudentDiscipline, "Student is already enrolled");
        else
            Verifier.verify().isNotNull(existingStudentDiscipline, "Student is not enrolled");
    }
}

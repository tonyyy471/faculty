package com.example.university.services;

import com.example.university.models.Discipline;
import com.example.university.models.Teacher;
import com.example.university.models.dtos.FacultyEntityDTO;
import com.example.university.repositiories.DisciplineRepository;
import com.example.university.repositiories.FacultyMemberRepository;
import com.example.university.services.utils.Verifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

@Service
public class DisciplineServiceImpl implements DisciplineService {
    private final DisciplineRepository disciplineRepository;
    private final FacultyMemberRepository facultyMemberRepository;

    @Autowired
    public DisciplineServiceImpl(DisciplineRepository disciplineRepository, FacultyMemberRepository facultyMemberRepository) {
        this.disciplineRepository = disciplineRepository;
        this.facultyMemberRepository = facultyMemberRepository;
    }

    @Override
    public void register(Discipline discipline) throws SQLException {
        String name = discipline.getName();
        Verifier.verify().isNotNull(name, "Discipline name is not provided");
        Verifier.verify().isNotEmptyOrBlank(name, "Discipline name cannot be empty or blank");

        Integer credit = discipline.getCredit();
        Verifier.verify().isNotNull(credit, "Discipline credit is not provided");
        Verifier.verify().isInRange(credit, 1, 5, "Discipline credit cannot be less than %d and more than %d");

        Integer teacherId = discipline.getTeacherId();
        Verifier.verify().isNotNull(teacherId, "Discipline teacher id is not provided");

        Teacher teacher = facultyMemberRepository.getTeacherBy(teacherId);
        Verifier.verify().isNotNull(teacher, "Teacher does not exist");

        Discipline existingDiscipline = disciplineRepository.getDisciplineBy(name);
        Verifier.verify().isNull(existingDiscipline, String.format("Discipline with name %s already exists", name));

        disciplineRepository.save(discipline);
    }

    @Override
    public List<FacultyEntityDTO> getTopThree() throws SQLException {
        List<FacultyEntityDTO> disciplines = disciplineRepository.getTopThree();
        return disciplines;
    }

    @Override
    public List<FacultyEntityDTO> getHavingOneOrMoreStudents() throws SQLException {
        List<FacultyEntityDTO> disciplines = disciplineRepository.getHavingOneOrMoreStudents();
        return disciplines;
    }

    @Override
    public List<FacultyEntityDTO> get() throws SQLException {
        List<FacultyEntityDTO> disciplines = disciplineRepository.get();
        return disciplines;
    }
}

package com.example.university.services.utils;

import com.example.university.services.exceptions.*;

public class Verifier {
    private static Verifier verifier;

    private Verifier() {
    }

    public static Verifier verify() {
        if (verifier == null)
            verifier = new Verifier();
        return verifier;
    }

    public void isNotEmptyOrBlank(String parameter, String exceptionMessage) {
        if (parameter.isEmpty() || parameter.isBlank())
            throw new NotAcceptableException(exceptionMessage);
    }

    public void isInRange(Integer parameter, int lowest, int highest, String exceptionMessage) {
        if (parameter < lowest || parameter > highest)
            throw new NotAcceptableException(String.format(exceptionMessage, lowest, highest));
    }

    public void isNotNull(Object parameter, String exceptionMessage) {
        if (parameter == null || parameter.equals("null"))
            throw new NotAcceptableException(exceptionMessage);
    }

    public void isNull(Object parameter, String exceptionMessage) {
        if (parameter != null)
            throw new NotAcceptableException(exceptionMessage);
    }
}

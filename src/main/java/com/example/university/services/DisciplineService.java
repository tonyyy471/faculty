package com.example.university.services;

import com.example.university.models.Discipline;
import com.example.university.models.dtos.FacultyEntityDTO;

import java.sql.SQLException;
import java.util.List;

public interface DisciplineService {
    void register(Discipline discipline) throws SQLException;

    List<FacultyEntityDTO> getTopThree()throws SQLException;

    List<FacultyEntityDTO> getHavingOneOrMoreStudents() throws SQLException;

    List<FacultyEntityDTO> get()throws SQLException;
}

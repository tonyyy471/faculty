package com.example.university.controllers;

import com.example.university.models.Discipline;
import com.example.university.models.dtos.FacultyEntityDTO;
import com.example.university.services.DisciplineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/api/disciplines")
public class DisciplineRestController {
    private final DisciplineService disciplineService;

    @Autowired
    public DisciplineRestController(DisciplineService disciplineService) {
        this.disciplineService = disciplineService;
    }

    @PostMapping
    public void register(@RequestBody Discipline discipline) throws SQLException {
        disciplineService.register(discipline);
    }

    @GetMapping
    public ResponseEntity<List<FacultyEntityDTO>> get() throws SQLException {
        List<FacultyEntityDTO> disciplines = disciplineService.get();
        return new ResponseEntity<>(disciplines, HttpStatus.OK);
    }

    @GetMapping("/top-three")
    public ResponseEntity<List<FacultyEntityDTO>> getTopThree() throws SQLException {
        List<FacultyEntityDTO> disciplines = disciplineService.getTopThree();
        return new ResponseEntity<>(disciplines, HttpStatus.OK);
    }

    @GetMapping("/having-one-or-more-students")
    public ResponseEntity<List<FacultyEntityDTO>> getHavingOneOrMoreStudents() throws SQLException {
        List<FacultyEntityDTO> disciplines = disciplineService.getHavingOneOrMoreStudents();
        return new ResponseEntity<>(disciplines, HttpStatus.OK);
    }
}

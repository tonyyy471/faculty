package com.example.university.controllers;

import com.example.university.models.Student;
import com.example.university.models.StudentDiscipline;
import com.example.university.models.Teacher;
import com.example.university.models.dtos.FacultyEntityDTO;
import com.example.university.services.FacultyMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class FacultyMemberRestController {
    private final FacultyMemberService facultyMemberService;

    @Autowired
    public FacultyMemberRestController(FacultyMemberService facultyMemberService) {
        this.facultyMemberService = facultyMemberService;
    }

    @PostMapping("/teachers")
    public void register(@RequestBody Teacher teacher) throws SQLException {
        facultyMemberService.register(teacher);
    }

    @PostMapping("/students")
    public void register(@RequestBody Student student) throws SQLException {
        facultyMemberService.register(student);
    }

    @PostMapping("/students-disciplines")
    public void enroll(@RequestBody StudentDiscipline studentDiscipline) throws SQLException {
        facultyMemberService.enroll(studentDiscipline);
    }

    @DeleteMapping("/students-disciplines")
    public void disenroll(@RequestBody StudentDiscipline studentDiscipline) throws SQLException {
        facultyMemberService.disenroll(studentDiscipline);
    }

    @GetMapping("/teachers")
    public ResponseEntity<List<FacultyEntityDTO>> getTeachers() throws SQLException {
        List<FacultyEntityDTO> teachers = facultyMemberService.getTeachers();
        return new ResponseEntity<>(teachers, HttpStatus.OK);
    }

    @GetMapping("/teachers/disciplines-count")
    public ResponseEntity<List<FacultyEntityDTO>> getTeachersAndTheirDisciplinesCount() throws SQLException {
        List<FacultyEntityDTO> teachers = facultyMemberService.getTeachersAndTheirDisciplinesCount();
        return new ResponseEntity<>(teachers, HttpStatus.OK);
    }

    @GetMapping("/students/courses")
    public ResponseEntity<List<FacultyEntityDTO>> getStudentsAndTheirCourses() throws SQLException {
        List<FacultyEntityDTO> students = facultyMemberService.getStudentsAndTheirCourses();
        return new ResponseEntity<>(students, HttpStatus.OK);
    }

    @GetMapping("/students/disciplines")
    public ResponseEntity<List<FacultyEntityDTO>> getStudentsAndTheirDisciplines() throws SQLException {
        List<FacultyEntityDTO> students = facultyMemberService.getStudentsAndTheirDisciplines();
        return new ResponseEntity<>(students, HttpStatus.OK);
    }

    @GetMapping("/students/credits")
    public ResponseEntity<List<FacultyEntityDTO>> getStudentsAndTheirCredits() throws SQLException {
        List<FacultyEntityDTO> students = facultyMemberService.getStudentsAndTheirCredits();
        return new ResponseEntity<>(students, HttpStatus.OK);
    }

    @GetMapping("/teachers/disciplines/students-count")
    public ResponseEntity<List<FacultyEntityDTO>> getTeachersTheirDisciplinesAndTheirStudentsCount() throws SQLException {
        List<FacultyEntityDTO> students = facultyMemberService.getTeachersTheirDisciplinesAndTheirStudentsCount();
        return new ResponseEntity<>(students, HttpStatus.OK);
    }

    @GetMapping("/teachers/top-three")
    public ResponseEntity<List<FacultyEntityDTO>> getTopThreeTeachers() throws SQLException {
        List<FacultyEntityDTO> teachers = facultyMemberService.getTopThreeTeachers();
        return new ResponseEntity<>(teachers, HttpStatus.OK);
    }
}

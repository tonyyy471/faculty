package com.example.university.repositiories;

import com.example.university.models.Discipline;
import com.example.university.models.dtos.FacultyEntityDTO;
import com.example.university.models.utils.ModelMapper;
import com.example.university.repositiories.utils.ConnectionWrapper;
import com.example.university.repositiories.utils.Queries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class DisciplineRepositoryImpl implements DisciplineRepository {
    private final ConnectionWrapper connectionWrapper;

    @Autowired
    public DisciplineRepositoryImpl(ConnectionWrapper connectionWrapper) {
        this.connectionWrapper = connectionWrapper;
    }

    @Override
    public void save(Discipline discipline) throws SQLException {
        try (PreparedStatement statement = connectionWrapper.getConnection().prepareStatement(Queries.SAVE_DISCIPLINE)) {
            statement.setString(1, discipline.getName());
            statement.setInt(2, discipline.getCredit());
            statement.setInt(3, discipline.getTeacherId());
            statement.executeUpdate();
        }
    }

    @Override
    public Discipline getDisciplineBy(String name) throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = connectionWrapper.getConnection().prepareStatement(Queries.GET_DISCIPLINE_BY_NAME)) {
            statement.setString(1, name);
            resultSet = statement.executeQuery();
        }
        return resultSet.first() ? ModelMapper.getInstance().toDiscipline(resultSet) : null;
    }

    @Override
    public Discipline getDisciplineBy(int id) throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = connectionWrapper.getConnection().prepareStatement(Queries.GET_DISCIPLINE_BY_ID)) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
        }
        return resultSet.first() ? ModelMapper.getInstance().toDiscipline(resultSet) : null;
    }

    @Override
    public List<FacultyEntityDTO> getTopThree() throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = connectionWrapper.getConnection().prepareStatement(Queries.GET_TOP_THREE_DISCIPLINES)) {
            resultSet = statement.executeQuery();
        }
        return ModelMapper.getInstance().toDisciplinesWithStudentsCount(resultSet);
    }

    @Override
    public List<FacultyEntityDTO> getHavingOneOrMoreStudents() throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = connectionWrapper.getConnection().prepareStatement(Queries.GET_DISCIPLINES_HAVING_MORE_THAN_ONE_STUDENT)) {
            resultSet = statement.executeQuery();
        }
        return ModelMapper.getInstance().toDisciplinesWithStudentsCount(resultSet);
    }

    @Override
    public List<FacultyEntityDTO> get() throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = connectionWrapper.getConnection().prepareStatement(Queries.GET_DISCIPLINES)) {
            resultSet = statement.executeQuery();
        }
        return ModelMapper.getInstance().toDisciplines(resultSet);
    }
}

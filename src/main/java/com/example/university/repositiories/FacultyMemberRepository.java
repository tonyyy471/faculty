package com.example.university.repositiories;

import com.example.university.models.Student;
import com.example.university.models.StudentDiscipline;
import com.example.university.models.Teacher;
import com.example.university.models.dtos.FacultyEntityDTO;

import java.sql.SQLException;
import java.util.List;

public interface FacultyMemberRepository {

    void save(Teacher teacher) throws SQLException;

    void save(Student student) throws SQLException;

    void save(StudentDiscipline studentDiscipline) throws SQLException;

    Teacher getTeacherBy(int id) throws SQLException;

    Student getStudentBy(int id) throws SQLException;

    StudentDiscipline getStudentDisciplineBy(int studentId, int disciplineId) throws SQLException;

    void delete(StudentDiscipline studentDiscipline) throws SQLException;

    List<FacultyEntityDTO> getTeachersAndTheirDisciplinesCount() throws SQLException;

    List<FacultyEntityDTO> getStudentsAndTheirCourses() throws SQLException;

    List<FacultyEntityDTO> getStudentsAndTheirDisciplines() throws SQLException;

    List<FacultyEntityDTO> getStudentsAndTheirCredits() throws SQLException;

    List<FacultyEntityDTO> getTeachersTheirDisciplinesAndTheirStudentsCount()throws SQLException;

    List<FacultyEntityDTO> getTopThreeTeachers()throws SQLException;

    List<FacultyEntityDTO> getTeachers()throws SQLException;
}

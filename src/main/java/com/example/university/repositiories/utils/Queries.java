package com.example.university.repositiories.utils;

public class Queries {
    public static final String SAVE_TEACHER = "insert into teachers(name, title) values (?,?)";

    public static final String SAVE_STUDENT = "insert into students(name, course) values (?,?)";

    public static final String GET_TEACHER_BY_ID = "select name, title from teachers where id = ?";

    public static final String GET_STUDENT_BY_ID = "select name, course from students where id = ?";

    public static final String SAVE_DISCIPLINE = "insert into disciplines(name, credit, teacher_id) values (?,?,?)";

    public static final String GET_DISCIPLINE_BY_NAME = "select name, credit, teacher_id from disciplines where name = ?";

    public static final String GET_DISCIPLINE_BY_ID = "select name, credit, teacher_id from disciplines where id = ?";

    public static final String GET_STUDENT_DISCIPLINE_BY_ID =
            "select student_id, discipline_id from students_disciplines where student_id = ? and discipline_id = ?";

    public static final String SAVE_STUDENT_DISCIPLINE = "insert into students_disciplines(student_id, discipline_id) values (?,?)";

    public static final String DELETE_STUDENT_DISCIPLINE = "delete from students_disciplines where student_id = ? and discipline_id = ?";

    public static final String GET_TEACHERS =
            "select teachers.id         as teacher_id,\n" +
                    "       teachers.name       as teacher_name\n" +
                    "from teachers;";

    public static final String GET_DISCIPLINES =
            "select disciplines.id         as discipline_id,\n" +
                    "       disciplines.name       as discipline_name\n" +
                    "from disciplines;";

    //1. Общ списък на преподаватели и студенти, подреден по азбучен ред
    //1a Имената на преподавателите и колко дисциплини води всеки преподавател
    public static final String GET_TEACHERS_AND_THEIR_DISCIPLINES_COUNT =
            "select teachers.id         as teacher_id,\n" +
                    "       teachers.name       as teacher_name,\n" +
                    "       count(d.teacher_id) as disciplines_count\n" +
                    "from teachers\n" +
                    "         left join disciplines d on teachers.id = d.teacher_id\n" +
                    "group by teachers.id\n" +
                    "order by teacher_name;";

    //1б Имената на студентите и в кой курс са
    public static final String GET_STUDENTS_AND_THEIR_COURSES =
            "select students.id     as student_id,\n" +
                    "       students.name   as student_name,\n" +
                    "       students.course as student_course\n" +
                    "from students\n" +
                    "order by student_name;";

    //2. Списък на студентите и кои дисциплини е избрал/а
    public static final String GET_STUDENTS_AND_THEIR_DISCIPLINES =
            "select students.id   as faculty_member_id,\n" +
                    "       students.name as faculty_member_name,\n" +
                    "       d.id          as discipline_id,\n" +
                    "       d.name        as discipline_name\n" +
                    "from students\n" +
                    "         left join students_disciplines sd on students.id = sd.student_id\n" +
                    "         left join disciplines d on sd.discipline_id = d.id\n" +
                    "order by students.name;";

    //3. Списък на студентите и броя кредити, които е събрал/а
    public static final String GET_STUDENTS_AND_THEIR_CREDITS =
            "select students.id   as student_id,\n" +
                    "       students.name as student_name,\n" +
                    "       sum(d.credit) as credits_count\n" +
                    "from students\n" +
                    "         left join students_disciplines sd on students.id = sd.student_id\n" +
                    "         left join disciplines d on sd.discipline_id = d.id\n" +
                    "group by students.id\n" +
                    "order by credits_count desc";

    //4. Списък на преподавателите, дисциплини, които води и броя студенти, които са се записали за всяка от дисциплините
    public static final String GET_TEACHERS_THEIR_DISCIPLINES_AND_THEIR_STUDENTS_COUNT =
            "select teachers.id             as faculty_member_id,\n" +
                    "       teachers.name           as faculty_member_name,\n" +
                    "       d.id                    as discipline_id,\n" +
                    "       d.name                  as discipline_name,\n" +
                    "       count(sd.discipline_id) as students_count\n" +
                    "from teachers\n" +
                    "         left join disciplines d on teachers.id = d.teacher_id\n" +
                    "         left join students_disciplines sd on d.id = sd.discipline_id\n" +
                    "group by teachers.id, d.id\n" +
                    "order by teachers.id;";

    //5. Списък на първите 3 дисциплини с най-много записали се студенти
    public static final String GET_TOP_THREE_DISCIPLINES =
            "select disciplines.id          as discipline_id,\n" +
                    "       disciplines.name        as discipline_name,\n" +
                    "       count(sd.discipline_id) as students_count\n" +
                    "from disciplines\n" +
                    "         left join students_disciplines sd on disciplines.id = sd.discipline_id\n" +
                    "group by disciplines.id\n" +
                    "order by students_count desc\n" +
                    "limit 3;";

    //6. Списък на първите 3-ма преподаватели с най-много записали се студенти по всички дисциплини, които те водят
    public static final String GET_TOP_THREE_TEACHERS =
            "select teachers.id             as teacher_id,\n" +
                    "       teachers.name           as teacher_name,\n" +
                    "       count(sd.discipline_id) as students_count\n" +
                    "from teachers\n" +
                    "         left join disciplines d on teachers.id = d.teacher_id\n" +
                    "         left join students_disciplines sd on d.id = sd.discipline_id\n" +
                    "group by teachers.id\n" +
                    "order by students_count desc\n" +
                    "limit 3;";

    //7. Списък на дисциплините с повече от един студент
    public static final String GET_DISCIPLINES_HAVING_MORE_THAN_ONE_STUDENT =
            "select disciplines.id          as discipline_id,\n" +
                    "       disciplines.name        as discipline_name,\n" +
                    "       count(sd.discipline_id) as students_count\n" +
                    "from disciplines\n" +
                    "         left join students_disciplines sd on disciplines.id = sd.discipline_id\n" +
                    "group by disciplines.id\n" +
                    "having students_count > 1;";
}

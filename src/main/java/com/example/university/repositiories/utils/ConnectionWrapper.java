package com.example.university.repositiories.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Objects;

@Component
@PropertySource("classpath:application.properties")
public class ConnectionWrapper {
    private Connection connection;

    @Autowired
    public ConnectionWrapper(Environment e) throws SQLException {
        initializeConnection(e);
    }

    public Connection getConnection() {
        return connection;
    }

    private void initializeConnection(Environment e) throws SQLException {
        this.connection = DriverManager.getConnection(Objects.requireNonNull(e.getProperty("spring.datasource.url")),
                e.getProperty("spring.datasource.username"), e.getProperty("spring.datasource.password"));
    }
}

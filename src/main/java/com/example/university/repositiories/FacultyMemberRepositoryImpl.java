package com.example.university.repositiories;

import com.example.university.models.Student;
import com.example.university.models.StudentDiscipline;
import com.example.university.models.Teacher;
import com.example.university.models.dtos.FacultyEntityDTO;
import com.example.university.models.utils.ModelMapper;
import com.example.university.repositiories.utils.ConnectionWrapper;
import com.example.university.repositiories.utils.Queries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class FacultyMemberRepositoryImpl implements FacultyMemberRepository {
    private final ConnectionWrapper connectionWrapper;

    @Autowired
    public FacultyMemberRepositoryImpl(ConnectionWrapper connectionWrapper) {
        this.connectionWrapper = connectionWrapper;
    }

    @Override
    public void save(Teacher teacher) throws SQLException {
        try (PreparedStatement statement = connectionWrapper.getConnection().prepareStatement(Queries.SAVE_TEACHER)) {
            statement.setString(1, teacher.getName());
            statement.setString(2, String.valueOf(teacher.getTitle()));
            statement.executeUpdate();
        }
    }

    @Override
    public void save(Student student) throws SQLException {
        try (PreparedStatement statement = connectionWrapper.getConnection().prepareStatement(Queries.SAVE_STUDENT)) {
            statement.setString(1, student.getName());
            statement.setInt(2, student.getCourse());
            statement.executeUpdate();
        }
    }

    @Override
    public void save(StudentDiscipline studentDiscipline) throws SQLException {
        try (PreparedStatement statement = connectionWrapper.getConnection().prepareStatement(Queries.SAVE_STUDENT_DISCIPLINE)) {
            statement.setInt(1, studentDiscipline.getStudentId());
            statement.setInt(2, studentDiscipline.getDisciplineId());
            statement.executeUpdate();
        }
    }

    @Override
    public Teacher getTeacherBy(int id) throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = connectionWrapper.getConnection().prepareStatement(Queries.GET_TEACHER_BY_ID)) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
        }
        return resultSet.first() ? ModelMapper.getInstance().toTeacher(resultSet) : null;
    }

    @Override
    public Student getStudentBy(int id) throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = connectionWrapper.getConnection().prepareStatement(Queries.GET_STUDENT_BY_ID)) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
        }
        return resultSet.first() ? ModelMapper.getInstance().toStudent(resultSet) : null;
    }

    @Override
    public StudentDiscipline getStudentDisciplineBy(int studentId, int disciplineId) throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = connectionWrapper.getConnection().prepareStatement(Queries.GET_STUDENT_DISCIPLINE_BY_ID)) {
            statement.setInt(1, studentId);
            statement.setInt(2, disciplineId);
            resultSet = statement.executeQuery();
        }
        return resultSet.first() ? ModelMapper.getInstance().toStudentDiscipline(resultSet) : null;
    }

    @Override
    public void delete(StudentDiscipline studentDiscipline) throws SQLException {
        try (PreparedStatement statement = connectionWrapper.getConnection().prepareStatement(Queries.DELETE_STUDENT_DISCIPLINE)) {
            statement.setInt(1, studentDiscipline.getStudentId());
            statement.setInt(2, studentDiscipline.getDisciplineId());
            statement.executeUpdate();
        }
    }

    @Override
    public List<FacultyEntityDTO> getTeachersAndTheirDisciplinesCount() throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = connectionWrapper.getConnection().prepareStatement(Queries.GET_TEACHERS_AND_THEIR_DISCIPLINES_COUNT)) {
            resultSet = statement.executeQuery();
        }
        return ModelMapper.getInstance().toTeachersWithDisciplinesCount(resultSet);
    }

    @Override
    public List<FacultyEntityDTO> getStudentsAndTheirCourses() throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = connectionWrapper.getConnection().prepareStatement(Queries.GET_STUDENTS_AND_THEIR_COURSES)) {
            resultSet = statement.executeQuery();
        }
        return ModelMapper.getInstance().toStudentsWithCourses(resultSet);
    }

    @Override
    public List<FacultyEntityDTO> getStudentsAndTheirDisciplines() throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = connectionWrapper.getConnection().prepareStatement(Queries.GET_STUDENTS_AND_THEIR_DISCIPLINES)) {
            resultSet = statement.executeQuery();
        }
        return ModelMapper.getInstance().toFacultyMembersWithDisciplines(resultSet, true);
    }

    @Override
    public List<FacultyEntityDTO> getStudentsAndTheirCredits() throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = connectionWrapper.getConnection().prepareStatement(Queries.GET_STUDENTS_AND_THEIR_CREDITS)) {
            resultSet = statement.executeQuery();
        }
        return ModelMapper.getInstance().toStudentsWithCredits(resultSet);
    }

    @Override
    public List<FacultyEntityDTO> getTeachersTheirDisciplinesAndTheirStudentsCount() throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = connectionWrapper.getConnection().prepareStatement(Queries.GET_TEACHERS_THEIR_DISCIPLINES_AND_THEIR_STUDENTS_COUNT)) {
            resultSet = statement.executeQuery();
        }
        return ModelMapper.getInstance().toFacultyMembersWithDisciplines(resultSet, false);
    }

    @Override
    public List<FacultyEntityDTO> getTopThreeTeachers() throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = connectionWrapper.getConnection().prepareStatement(Queries.GET_TOP_THREE_TEACHERS)) {
            resultSet = statement.executeQuery();
        }
        return ModelMapper.getInstance().toTeachersWithStudentsCount(resultSet);
    }

    @Override
    public List<FacultyEntityDTO> getTeachers() throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = connectionWrapper.getConnection().prepareStatement(Queries.GET_TEACHERS)) {
            resultSet = statement.executeQuery();
        }
        return ModelMapper.getInstance().toTeachers(resultSet);
    }
}

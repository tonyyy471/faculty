package com.example.university.repositiories;

import com.example.university.models.Discipline;
import com.example.university.models.dtos.FacultyEntityDTO;

import java.sql.SQLException;
import java.util.List;

public interface DisciplineRepository {
    void save(Discipline discipline) throws SQLException;

    Discipline getDisciplineBy(String name) throws SQLException;

    Discipline getDisciplineBy(int id) throws SQLException;

    List<FacultyEntityDTO> getTopThree()throws SQLException;

    List<FacultyEntityDTO> getHavingOneOrMoreStudents()throws SQLException;

    List<FacultyEntityDTO> get()throws SQLException;
}

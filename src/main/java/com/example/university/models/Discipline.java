package com.example.university.models;

public class Discipline {
    private String name;
    private Integer credit;
    private Integer teacherId;

    public Discipline(String name, Integer credit, Integer teacherId) {
        this.name = name;
        this.credit = credit;
        this.teacherId = teacherId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCredit() {
        return credit;
    }

    public void setCredit(Integer credit) {
        this.credit = credit;
    }

    public Integer getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }
}

package com.example.university.models;

public class Student extends FacultyMember {
    private Integer course;

    public Student(String name, Integer course) {
        super(name);
        this.course = course;
    }

    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }
}

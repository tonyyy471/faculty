package com.example.university.models;

public class StudentDiscipline {
    private Integer studentId;
    private Integer disciplineId;

    public StudentDiscipline(Integer studentId, Integer disciplineId) {
        this.studentId = studentId;
        this.disciplineId = disciplineId;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public Integer getDisciplineId() {
        return disciplineId;
    }

    public void setDisciplineId(Integer disciplineId) {
        this.disciplineId = disciplineId;
    }
}

package com.example.university.models;

public enum Title {
    ASSISTANT_PROFESSOR,
    CHIEF_ASSISTANT_PROFESSOR,
    DOCENT_PROFESSOR,
    PROFESSOR
}

package com.example.university.models;

public class FacultyMember {
    private String name;

    public FacultyMember(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

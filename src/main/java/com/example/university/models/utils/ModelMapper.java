package com.example.university.models.utils;

import com.example.university.models.*;
import com.example.university.models.dtos.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class ModelMapper {
    private static ModelMapper modelMapper;

    private ModelMapper() {
    }

    public static ModelMapper getInstance() {
        if (modelMapper == null)
            modelMapper = new ModelMapper();
        return modelMapper;
    }

    public List<FacultyEntityDTO> toTeachers(ResultSet resultSet) throws SQLException {
        List<FacultyEntityDTO> teachers = new ArrayList<>();
        while (resultSet.next())
            teachers.add(new FacultyEntityDTO(resultSet.getInt("teacher_id"),
                    resultSet.getString("teacher_name")));
        return teachers;
    }

    public List<FacultyEntityDTO> toDisciplines(ResultSet resultSet) throws SQLException {
        List<FacultyEntityDTO> disciplines = new ArrayList<>();
        while (resultSet.next())
            disciplines.add(new FacultyEntityDTO(resultSet.getInt("discipline_id"),
                    resultSet.getString("discipline_name")));
        return disciplines;
    }

    public Teacher toTeacher(ResultSet resultSet) throws SQLException {
        return new Teacher(resultSet.getString("name"), Title.valueOf(resultSet.getString("title")));
    }

    public Discipline toDiscipline(ResultSet resultSet) throws SQLException {
        return new Discipline(resultSet.getString("name"), resultSet.getInt("credit"), resultSet.getInt("teacher_id"));
    }

    public Student toStudent(ResultSet resultSet) throws SQLException {
        return new Student(resultSet.getString("name"), resultSet.getInt("course"));
    }

    public StudentDiscipline toStudentDiscipline(ResultSet resultSet) throws SQLException {
        return new StudentDiscipline(resultSet.getInt("student_id"), resultSet.getInt("discipline_id"));
    }

    public List<FacultyEntityDTO> toTeachersWithDisciplinesCount(ResultSet resultSet) throws SQLException {
        List<FacultyEntityDTO> teachers = new ArrayList<>();
        while (resultSet.next())
            teachers.add(new TeacherWithDisciplinesCountDTO(resultSet.getInt("teacher_id"),
                    resultSet.getString("teacher_name"), resultSet.getInt("disciplines_count")));
        return teachers;
    }

    public List<FacultyEntityDTO> toTeachersWithStudentsCount(ResultSet resultSet) throws SQLException {
        List<FacultyEntityDTO> teachers = new ArrayList<>();
        while (resultSet.next())
            teachers.add(new TeacherWithStudentsCountDTO(resultSet.getInt("teacher_id"),
                    resultSet.getString("teacher_name"), resultSet.getInt("students_count")));
        return teachers;
    }

    public List<FacultyEntityDTO> toStudentsWithCourses(ResultSet resultSet) throws SQLException {
        List<FacultyEntityDTO> students = new ArrayList<>();
        while (resultSet.next())
            students.add(new StudentWithCourseDTO(resultSet.getInt("student_id"),
                    resultSet.getString("student_name"), resultSet.getInt("student_course")));
        return students;
    }

    public List<FacultyEntityDTO> toStudentsWithCredits(ResultSet resultSet) throws SQLException {
        List<FacultyEntityDTO> students = new ArrayList<>();
        while (resultSet.next())
            students.add(new StudentWithCreditsCountDTO(resultSet.getInt("student_id"),
                    resultSet.getString("student_name"), resultSet.getInt("credits_count")));
        return students;
    }

    public List<FacultyEntityDTO> toDisciplinesWithStudentsCount(ResultSet resultSet) throws SQLException {
        List<FacultyEntityDTO> disciplines = new ArrayList<>();
        while (resultSet.next())
            disciplines.add(new DisciplineDTO(resultSet.getInt("discipline_id"),
                    resultSet.getString("discipline_name"), resultSet.getInt("students_count")));
        return disciplines;
    }

    public List<FacultyEntityDTO> toFacultyMembersWithDisciplines(ResultSet resultSet, boolean areMembersStudents) throws SQLException {
        Map<Integer, DisciplinableFacultyMemberDTO> students = new HashMap<>();
        Map<Integer, FacultyEntityDTO> disciplines = new HashMap<>();

        while (resultSet.next()) {
            int studentId = resultSet.getInt("faculty_member_id");
            DisciplinableFacultyMemberDTO student;

            if (students.containsKey(studentId))
                student = students.get(studentId);
            else {
                student = new DisciplinableFacultyMemberDTO(studentId, resultSet.getString("faculty_member_name"), new ArrayList<>());
                students.put(studentId, student);
            }

            if (hasDiscipline(resultSet)) {
                int disciplineId = resultSet.getInt("discipline_id");
                FacultyEntityDTO discipline;

                if (disciplines.containsKey(disciplineId))
                    discipline = disciplines.get(disciplineId);
                else {
                    discipline = areMembersStudents ? new FacultyEntityDTO(disciplineId, resultSet.getString("discipline_name")) :
                            new DisciplineDTO(disciplineId, resultSet.getString("discipline_name"), resultSet.getInt("students_count"));
                    disciplines.put(disciplineId, discipline);
                }

                student.getDisciplines().add(discipline);
            }
        }

        return new ArrayList<>(students.values());
    }

    private boolean hasDiscipline(ResultSet resultSet) throws SQLException {
        return resultSet.getString("discipline_id") != null;
    }
}

package com.example.university.models;

public class Teacher extends FacultyMember {
    private Title title;

    public Teacher(String name, Title title) {
        super(name);
        this.title = title;
    }

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }
}

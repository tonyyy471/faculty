package com.example.university.models.dtos;

public class StudentWithCreditsCountDTO extends FacultyEntityDTO {
    private final Integer creditsCount;

    public StudentWithCreditsCountDTO(Integer id, String name, Integer creditsCount) {
        super(id, name);
        this.creditsCount = creditsCount;
    }

    public Integer getCreditsCount() {
        return creditsCount;
    }
}

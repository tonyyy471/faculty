package com.example.university.models.dtos;

public class StudentWithCourseDTO extends FacultyEntityDTO {
    private final Integer course;

    public StudentWithCourseDTO(Integer id, String name, Integer course) {
        super(id, name);
        this.course = course;
    }

    public Integer getCourse() {
        return course;
    }
}

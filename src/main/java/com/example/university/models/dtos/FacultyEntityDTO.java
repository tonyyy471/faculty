package com.example.university.models.dtos;

public class FacultyEntityDTO {
    private final Integer id;
    private final String name;

    public FacultyEntityDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}

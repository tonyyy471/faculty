package com.example.university.models.dtos;

public class TeacherWithDisciplinesCountDTO extends FacultyEntityDTO {
    private final Integer disciplinesCount;

    public TeacherWithDisciplinesCountDTO(Integer id, String name, Integer disciplinesCount) {
        super(id, name);
        this.disciplinesCount = disciplinesCount;
    }

    public Integer getDisciplinesCount() {
        return disciplinesCount;
    }
}

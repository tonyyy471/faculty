package com.example.university.models.dtos;

import java.util.List;

public class DisciplinableFacultyMemberDTO extends FacultyEntityDTO {
    private final List<FacultyEntityDTO> disciplines;

    public DisciplinableFacultyMemberDTO(Integer id, String name, List<FacultyEntityDTO> disciplines) {
        super(id, name);
        this.disciplines = disciplines;
    }

    public List<FacultyEntityDTO> getDisciplines() {
        return disciplines;
    }
}

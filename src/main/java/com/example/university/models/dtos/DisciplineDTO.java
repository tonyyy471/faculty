package com.example.university.models.dtos;

public class DisciplineDTO extends FacultyEntityDTO {
    private final Integer studentsCount;

    public DisciplineDTO(Integer id, String name, Integer studentsCount) {
        super(id, name);
        this.studentsCount = studentsCount;
    }

    public Integer getStudentsCount() {
        return studentsCount;
    }
}

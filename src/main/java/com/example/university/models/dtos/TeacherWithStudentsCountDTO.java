package com.example.university.models.dtos;

public class TeacherWithStudentsCountDTO extends FacultyEntityDTO {
    private final Integer studentsCount;

    public TeacherWithStudentsCountDTO(Integer id, String name, Integer studentsCount) {
        super(id, name);
        this.studentsCount = studentsCount;
    }

    public Integer getStudentsCount() {
        return studentsCount;
    }
}
